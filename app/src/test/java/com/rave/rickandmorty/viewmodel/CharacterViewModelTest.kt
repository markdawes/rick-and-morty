package com.rave.rickandmorty.viewmodel

import com.rave.rickandmorty.model.RickMortyRepo
import com.rave.rickandmorty.model.local.RickCharacter
import com.rave.rickandmorty.model.remote.NetworkResponse
import com.rave.rickandmorty.model.remote.dtos.character.CharacterResponse
import com.rave.rickandmorty.model.utiltest.CoroutinesTestExtension
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.Job
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.RegisterExtension

@OptIn(ExperimentalCoroutinesApi::class)
internal class CharacterViewModelTest {

    @RegisterExtension
    private val extension = CoroutinesTestExtension()

    private val repo = mockk<RickMortyRepo>()

    private val viewModel: CharacterViewModel = CharacterViewModel(repo)

    @Test
    @DisplayName("Testing ViewModel State")
    fun TestViewModelState() = runTest(extension.dispatcher) {

        val expectedResponse = NetworkResponse.SuccessfulCharacter(
            listOf(RickCharacter(
                name = "Spongebob",
                created = "",
                episode = emptyList(),
                gender = "male",
                id = 0,
                image = "",
                location = emptyMap(),
                species = "sponge",
                status = "alive",
                type = "",
                url = ""
            ))
        )

        // Given
        coEvery { repo.getCharacters() } coAnswers { expectedResponse }
        // When
        val characterResponse = viewModel.getCharacters()

        // Then
        Assertions.assertNotEquals(null, characterResponse)
        Assertions.assertTrue(characterResponse is Job)
    }
}