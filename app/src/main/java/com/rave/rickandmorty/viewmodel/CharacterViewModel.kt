package com.rave.rickandmorty.viewmodel

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.rave.rickandmorty.model.RickMortyRepo
import com.rave.rickandmorty.model.remote.NetworkResponse
import com.rave.rickandmorty.views.characterdetailsscreen.CharacterDetailsState
import com.rave.rickandmorty.views.characterscreen.CharacterScreenState
import com.rave.rickandmorty.views.episodescreen.EpisodeScreenState
import com.rave.rickandmorty.views.locationscreen.LocationScreenState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

/**
 * Character view model.
 *
 * @property repo
 * @constructor Create empty Character view model
 */
class CharacterViewModel(
    private val repo: RickMortyRepo
) : ViewModel() {
    private val tag = "CharacterViewModel"

    private val _characters: MutableStateFlow<CharacterScreenState> =
        MutableStateFlow(CharacterScreenState())
    val characterState: StateFlow<CharacterScreenState> get() = _characters

    private val _characterDetailsState: MutableStateFlow<CharacterDetailsState> =
        MutableStateFlow(CharacterDetailsState())
    val characterDetailsState: StateFlow<CharacterDetailsState> get() = _characterDetailsState

    private val _episodeScreenState: MutableStateFlow<EpisodeScreenState> =
        MutableStateFlow(EpisodeScreenState())
    val episodeScreenState: StateFlow<EpisodeScreenState> get() = _episodeScreenState

    private val _locationScreenState: MutableStateFlow<LocationScreenState> =
        MutableStateFlow(LocationScreenState())
    val locationScreenState: StateFlow<LocationScreenState> get() = _locationScreenState

    /**
     * Get characters.
     *
     */
    fun getCharacters() = viewModelScope.launch {
        _characters.update { it.copy(isLoading = true) }
        val characters = repo.getCharacters()
        handleResults(characters)
    }

    /**
     * Get character details.
     *
     */
    fun getCharacterDetails(id: Int) = viewModelScope.launch {
        _characterDetailsState.update { it.copy(isLoading = true) }
        val characterDetails = repo.getDetailsFromCharacter(id)
        handleResults(characterDetails)
    }

    /**
     * Get episode details.
     *
     * @param id
     */
    fun getEpisodeDetails(id: Int) = viewModelScope.launch {
        _episodeScreenState.update { it.copy(isLoading = true) }
        val episodeDetails = repo.getEpisodeFromCharacter(id)
        handleResults(episodeDetails)
    }

    /**
     * Get location details.
     *
     * @param id
     */
    fun getLocationDetails(id: Int) = viewModelScope.launch {
        _locationScreenState.update { it.copy(isLoading = true) }
        val locationDetails = repo.getLocationFromCharacter(id)
        handleResults(locationDetails)
    }

    /**
     * Handle results.
     *
     * @param networkResponse
     */
    private fun handleResults(networkResponse: NetworkResponse<*>) {
        when (networkResponse) {
            is NetworkResponse.Error -> _characters.update {
                it.copy(
                    isLoading = false,
                    error = networkResponse.message
                )
            }
            is NetworkResponse.SuccessfulCharacter -> _characters.update {
                it.copy(isLoading = false, characters = networkResponse.characters)
            }
            is NetworkResponse.SuccessfulDetails -> _characterDetailsState.update {
                it.copy(
                    isLoading = false,
                    rickCharacter = networkResponse.rickCharacter
                )
            }
            is NetworkResponse.SuccessfulEpisode -> _episodeScreenState.update {
                it.copy(
                    isLoading = false,
                    episode = networkResponse.episode
                )
            }
            is NetworkResponse.SuccessfulLocation -> _locationScreenState.update {
                it.copy(
                    isLoading = false,
                    location = networkResponse.location
                )
            }
            else -> Log.e(tag, "Error encountered")
        }
    }
}

/**
 * Vml factory.
 *
 * @property repo
 * @constructor Create empty V ml factory
 */
class VMlFactory(
    private val repo: RickMortyRepo
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(CharacterViewModel::class.java)) {
            return CharacterViewModel(repo) as T
        } else {
            @Suppress("useRequire")
            throw IllegalArgumentException("Illegal Argument")
        }
    }
}
