package com.rave.rickandmorty.model.remote

import com.rave.rickandmorty.model.local.Episode
import com.rave.rickandmorty.model.local.Location
import com.rave.rickandmorty.model.local.RickCharacter

/**
 * Network response.
 *
 * @param T
 * @constructor Create empty Network response
 */
sealed class NetworkResponse<T> {

    /**
     * Successful character.
     *
     * @property characters
     * @constructor Create empty Successful character
     */
    data class SuccessfulCharacter(
        val characters: List<RickCharacter>
    ) : NetworkResponse<List<RickCharacter>>()

    /**
     * Loading.
     *
     * @constructor Create empty Loading
     */
    object Loading : NetworkResponse<Unit>()

    /**
     * Successful details.
     *
     * @property rickCharacter
     * @constructor Create empty Successful details
     */
    data class SuccessfulDetails(
        val rickCharacter: RickCharacter
    ) : NetworkResponse<RickCharacter>()

    /**
     * Successful episode.
     *
     * @property episode
     * @constructor Create empty Successful episode
     */
    data class SuccessfulEpisode(
        val episode: Episode
    ) : NetworkResponse<Episode>()

    /**
     * Successful location.
     *
     * @property location
     * @constructor Create empty Successful location
     */
    data class SuccessfulLocation(
        val location: Location
    ) : NetworkResponse<Location>()

    /**
     * Error.
     *
     * @property message
     * @constructor Create empty Error
     */
    data class Error(
        val message: String
    ) : NetworkResponse<String>()
}
