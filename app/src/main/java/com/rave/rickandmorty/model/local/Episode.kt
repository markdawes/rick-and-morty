package com.rave.rickandmorty.model.local

/**
 * Episode.
 *
 * @property airDate
 * @property characters
 * @property created
 * @property episode
 * @property id
 * @property name
 * @property url
 * @constructor Create empty Episode
 */
data class Episode(
    val airDate: String,
    val characters: List<String>,
    val created: String,
    val episode: String,
    val id: Int,
    val name: String,
    val url: String
)
