package com.rave.rickandmorty.model.mapper

import com.rave.rickandmorty.model.local.Location
import com.rave.rickandmorty.model.remote.dtos.location.LocationDTO

/**
 * Location mapper.
 *
 * @constructor Create empty Location mapper
 */
class LocationMapper : Mapper<LocationDTO, Location> {
    override fun invoke(dto: LocationDTO): Location = with(dto) {
        Location(
            created = created,
            dimension = dimension,
            id = id,
            name = name,
            residents = residents,
            type = type,
            url = url
        )
    }
}
