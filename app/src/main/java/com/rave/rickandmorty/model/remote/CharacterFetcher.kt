package com.rave.rickandmorty.model.remote

import com.rave.rickandmorty.model.remote.dtos.character.CharacterDTO
import com.rave.rickandmorty.model.remote.dtos.character.CharacterResponse
import com.rave.rickandmorty.model.remote.dtos.episode.EpisodeDTO
import com.rave.rickandmorty.model.remote.dtos.location.LocationDTO
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * Character fetcher.
 *
 * @constructor Create empty Character fetcher
 */
interface CharacterFetcher {

    @GET(CHARACTER_ENDPOINT)
    fun getAllCharacters(): Call<CharacterResponse>

    @GET("character/{Id}")
    fun getDetailsFromCharacter(@Path("Id") characterId: Int): Call<CharacterDTO>

    @GET("episode/{Id}")
    fun getEpisode(@Path("Id") episodeId: Int): Call<EpisodeDTO>

    @GET("location/{Id}")
    fun getLocation(@Path("Id") locationId: Int): Call<LocationDTO>

    companion object {
        private const val CHARACTER_ENDPOINT = "character/1,2,3,4,5,6,7,8,9,10,11,12,13,14,15"
    }
}
