package com.rave.rickandmorty.model.remote

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create

/**
 * Object containing all data associated with the retrofit instance.
 *
 * @constructor Create empty Retrofit object
 */
// @OptIn(ExperimentalSerializationApi::class)
object RetrofitObject {
    // private const val VERSION = "/api/"
    private const val BASE_URL = "https://rickandmortyapi.com/api/"

    // private val mediaType = "application/json".toMediaType()

    private val retrofit: Retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        // .addConverterFactory(Json.asConverterFactory(mediaType))
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    /**
     * Get character fetcher.
     *
     * @return
     */
    fun getCharacterFetcher(): CharacterFetcher = retrofit.create()
}
