package com.rave.rickandmorty.model.local

/**
 * Location.
 *
 * @property created
 * @property dimension
 * @property id
 * @property name
 * @property residents
 * @property type
 * @property url
 * @constructor Create empty Location
 */
data class Location(
    val created: String,
    val dimension: String,
    val id: Int,
    val name: String,
    val residents: List<String>,
    val type: String,
    val url: String
)
