package com.rave.rickandmorty.model.remote.dtos.location

import kotlinx.serialization.Serializable

/**
 * Location dto.
 *
 * @property created
 * @property dimension
 * @property id
 * @property name
 * @property residents
 * @property type
 * @property url
 * @constructor Create empty Location dto
 */
@Serializable
data class LocationDTO(
    val created: String,
    val dimension: String,
    val id: Int,
    val name: String,
    val residents: List<String>,
    val type: String,
    val url: String
)
