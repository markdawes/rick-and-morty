package com.rave.rickandmorty.model.remote.dtos.character

/**
 * The response object given when asking for a list of [Character]
 * objects.
 */
// @kotlinx.serialization.Serializable
class CharacterResponse : ArrayList<CharacterDTO>()
