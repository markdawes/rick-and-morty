package com.rave.rickandmorty.model.remote.dtos.episode

import kotlinx.serialization.Serializable

/**
 * Class to represent the response object recieved
 * when clicking a characters episode.
 *
 * @constructor Create empty Episode response
 */
@Serializable
data class EpisodeResponse(
    val episodes: List<EpisodeDTO> = emptyList()
)
