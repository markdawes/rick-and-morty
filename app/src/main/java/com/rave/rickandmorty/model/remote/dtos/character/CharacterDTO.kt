package com.rave.rickandmorty.model.remote.dtos.character

import kotlinx.serialization.Serializable

/**
 * Local representation of a character.
 *
 * @property created
 * @property episode
 * @property gender
 * @property id
 * @property image
 * @property location
 * @property origin
 * @property name
 * @property species
 * @property status
 * @property type
 * @property url
 * @constructor Create empty Character
 */
@Serializable
data class CharacterDTO(
    val created: String,
    val episode: List<String>,
    val gender: String,
    val id: Int,
    val image: String,
    // @Contextual
    val location: Map<String, out String>,
    val origin: Map<String, out String>,
    val name: String,
    val species: String,
    val status: String,
    val type: String,
    val url: String
)
