package com.rave.rickandmorty.model.remote.dtos.location

import kotlinx.serialization.Serializable

/**
 * Class to represent the response object recieved
 * when clicking a characters episode.
 *
 * @constructor Create empty Location response
 */
@Serializable
data class LocationResponse(
    val locations: List<LocationDTO> = emptyList()
)
