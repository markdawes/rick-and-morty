package com.rave.rickandmorty.model.remote.dtos.episode

import kotlinx.serialization.Serializable

/**
 * Local representation of an episode.
 *
 * @property air_date
 * @property characters
 * @property created
 * @property episode
 * @property id
 * @property name
 * @property url
 * @constructor Create empty Episode
 */
@Serializable
data class EpisodeDTO(
    @Suppress("ConstructorParameterNaming", "LambdaParameterNaming")
    val air_date: String,
    val characters: List<String>,
    val created: String,
    val episode: String,
    val id: Int,
    val name: String,
    val url: String
)
