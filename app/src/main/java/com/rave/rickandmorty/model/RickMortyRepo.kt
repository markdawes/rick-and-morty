package com.rave.rickandmorty.model

import com.rave.rickandmorty.model.local.Episode
import com.rave.rickandmorty.model.local.Location
import com.rave.rickandmorty.model.local.RickCharacter
import com.rave.rickandmorty.model.mapper.CharacterMapper
import com.rave.rickandmorty.model.mapper.EpisodeMapper
import com.rave.rickandmorty.model.mapper.LocationMapper
import com.rave.rickandmorty.model.remote.CharacterFetcher
import com.rave.rickandmorty.model.remote.NetworkResponse
import com.rave.rickandmorty.model.remote.dtos.character.CharacterDTO
import com.rave.rickandmorty.model.remote.dtos.character.CharacterResponse
import com.rave.rickandmorty.model.remote.dtos.episode.EpisodeDTO
import com.rave.rickandmorty.model.remote.dtos.location.LocationDTO
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response

/**
 * Rick morty repo.
 *
 * @property service
 * @property characterMapper
 * @property episodeMapper
 * @property locationMapper
 * @constructor Create empty Rick morty repo
 */
class RickMortyRepo(
    private val service: CharacterFetcher,
    private val characterMapper: CharacterMapper,
    private val episodeMapper: EpisodeMapper,
    private val locationMapper: LocationMapper
) {
    /**
     * Get characters.
     *
     * @return
     */
    suspend fun getCharacters(): NetworkResponse<*> = withContext(Dispatchers.IO) {
        val characterResponse: Response<CharacterResponse> =
            service.getAllCharacters().execute()

        return@withContext if (characterResponse.isSuccessful) {
            val characterResponse = characterResponse.body() ?: CharacterResponse()
            val characterList: List<RickCharacter> = characterResponse.map {
                characterMapper(it)
            }
            NetworkResponse.SuccessfulCharacter(characterList)
        } else {
            NetworkResponse.Error(characterResponse.message())
        }
    }

    /**
     * Get details from character.
     *
     * @return
     */
    suspend fun getDetailsFromCharacter(id: Int): NetworkResponse<*> = withContext(Dispatchers.IO) {
        val characterDetailsResponse: Response<CharacterDTO> =
            service.getDetailsFromCharacter(id).execute()

        return@withContext if (characterDetailsResponse.isSuccessful) {
            val characterDetailsResponse = characterDetailsResponse.body()
            val rickCharacter: RickCharacter = characterMapper(characterDetailsResponse!!)
            NetworkResponse.SuccessfulDetails(rickCharacter)
        } else {
            NetworkResponse.Error(characterDetailsResponse.message())
        }
    }

    /**
     * Get episode from character.
     *
     * @param id
     * @return
     */
    suspend fun getEpisodeFromCharacter(id: Int): NetworkResponse<*> = withContext(Dispatchers.IO) {
        val episodeResponse: Response<EpisodeDTO> =
            service.getEpisode(id).execute()

        return@withContext if (episodeResponse.isSuccessful) {
            val episodeResponse = episodeResponse.body()
            val episode: Episode = episodeMapper(episodeResponse!!)
            NetworkResponse.SuccessfulEpisode(episode)
        } else {
            NetworkResponse.Error(episodeResponse.message())
        }
    }

    /**
     * Get location from character.
     *
     * @param id
     * @return
     */
    suspend fun getLocationFromCharacter(id: Int): NetworkResponse<*> = withContext(Dispatchers.IO) {
        val locationResponse: Response<LocationDTO> =
            service.getLocation(id).execute()

        return@withContext if (locationResponse.isSuccessful) {
            val locationResponse = locationResponse.body()
            val location: Location = locationMapper(locationResponse!!)
            NetworkResponse.SuccessfulLocation(location)
        } else {
            NetworkResponse.Error(locationResponse.message())
        }
    }
}
