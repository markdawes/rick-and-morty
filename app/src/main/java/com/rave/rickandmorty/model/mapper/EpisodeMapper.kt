package com.rave.rickandmorty.model.mapper

import com.rave.rickandmorty.model.local.Episode
import com.rave.rickandmorty.model.remote.dtos.episode.EpisodeDTO

/**
 * Episode mapper.
 *
 * @constructor Create empty Episode mapper
 */
class EpisodeMapper : Mapper<EpisodeDTO, Episode> {
    override fun invoke(dto: EpisodeDTO): Episode = with(dto) {
        Episode(
            airDate = air_date,
            characters = characters,
            created = created,
            episode = episode,
            id = id,
            name = name,
            url = url
        )
    }
}
