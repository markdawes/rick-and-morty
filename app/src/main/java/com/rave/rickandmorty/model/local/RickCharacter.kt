package com.rave.rickandmorty.model.local

/**
 * Rick character.
 *
 * @property created
 * @property episode
 * @property gender
 * @property id
 * @property image
 * @property location
 * @property name
 * @property species
 * @property status
 * @property type
 * @property url
 * @constructor Create empty Rick character
 */
data class RickCharacter(
    val created: String,
    val episode: List<String>,
    val gender: String,
    val id: Int,
    val image: String,
    val location: Map<String, out String>,
    val name: String,
    val species: String,
    val status: String,
    val type: String,
    val url: String
)
