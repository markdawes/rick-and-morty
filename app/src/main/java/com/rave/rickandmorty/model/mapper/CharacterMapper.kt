package com.rave.rickandmorty.model.mapper

import com.rave.rickandmorty.model.local.RickCharacter
import com.rave.rickandmorty.model.remote.dtos.character.CharacterDTO

/**
 * Character mapper.
 *
 * @constructor Create empty Character mapper
 */
class CharacterMapper : Mapper<CharacterDTO, RickCharacter> {
    override fun invoke(dto: CharacterDTO): RickCharacter = with(dto) {
        RickCharacter(
            created = created,
            episode = episode,
            gender = gender,
            id = id,
            image = image,
            location = location,
            name = name,
            species = species,
            status = status,
            type = type,
            url = url
        )
    }
}
