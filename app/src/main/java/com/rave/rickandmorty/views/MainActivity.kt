package com.rave.rickandmorty.views

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.rave.rickandmorty.model.RickMortyRepo
import com.rave.rickandmorty.model.local.RickCharacter
import com.rave.rickandmorty.model.mapper.CharacterMapper
import com.rave.rickandmorty.model.mapper.EpisodeMapper
import com.rave.rickandmorty.model.mapper.LocationMapper
import com.rave.rickandmorty.model.remote.RetrofitObject
import com.rave.rickandmorty.ui.theme.LightBlue
import com.rave.rickandmorty.ui.theme.RickAndMortyTheme
import com.rave.rickandmorty.viewmodel.CharacterViewModel
import com.rave.rickandmorty.viewmodel.VMlFactory
import com.rave.rickandmorty.views.characterdetailsscreen.CharacterDetailsScreen
import com.rave.rickandmorty.views.characterdetailsscreen.CharacterDetailsState
import com.rave.rickandmorty.views.characterscreen.CharacterScreen
import com.rave.rickandmorty.views.characterscreen.CharacterScreenState
import com.rave.rickandmorty.views.episodescreen.EpisodeScreen
import com.rave.rickandmorty.views.episodescreen.EpisodeScreenState
import com.rave.rickandmorty.views.locationscreen.LocationScreen
import com.rave.rickandmorty.views.locationscreen.LocationScreenState

/**
 * Main activity - Entry point for the app.
 *
 * @constructor Create empty Main activity
 */
class MainActivity : ComponentActivity() {

    private val characterViewModel by viewModels<CharacterViewModel> {
        val repo = RickMortyRepo(
            RetrofitObject.getCharacterFetcher(),
            CharacterMapper(),
            EpisodeMapper(),
            LocationMapper()
        )
        VMlFactory(repo)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        characterViewModel.getCharacters()
        setContent {
            val characterState by characterViewModel.characterState.collectAsState()
            val characterDetailsState by characterViewModel.characterDetailsState.collectAsState()
            val episodeScreenState by characterViewModel.episodeScreenState.collectAsState()
            val locationScreenState by characterViewModel.locationScreenState.collectAsState()
            RickAndMortyTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = LightBlue
                ) {
                    RickMortyApp(
                        characterState,
                        characterDetailsState,
                        episodeScreenState,
                        locationScreenState,
                        onCharacterSelected = { selectedCharacter: RickCharacter ->
                            characterViewModel.getCharacterDetails(selectedCharacter.id)
                        },
                        onEpisodeSelected = { episodeNum: Int ->
                            characterViewModel.getEpisodeDetails(episodeNum)
                        },
                        onLocationSelected = { locationId: Int ->
                            characterViewModel.getLocationDetails(locationId)
                        }
                    )
                }
            }
        }
    }
}

@Composable
@Suppress("LongParameterList")
fun RickMortyApp(
    characters: CharacterScreenState,
    characterDetailsState: CharacterDetailsState,
    episodeScreenState: EpisodeScreenState,
    locationScreenState: LocationScreenState,
    onCharacterSelected: (rickCharacter: RickCharacter) -> Unit,
    onEpisodeSelected: (episodeNum: Int) -> Unit,
    onLocationSelected: (locationId: Int) -> Unit
) {
    val navController: NavHostController = rememberNavController()
    NavHost(
        navController = navController,
        startDestination = Screens.CharacterScreen.route
    ) {
        composable(Screens.CharacterScreen.route) {
            CharacterScreen(characters) { screen: Screens, rickCharacter: RickCharacter ->
                onCharacterSelected(rickCharacter)
                navController.navigate(screen.route)
            }
        }
        composable(Screens.CharacterDetailsScreen.route) {
            CharacterDetailsScreen(
                state = characterDetailsState,
                navigate = { screen: Screens, episodeNum: Int ->
                    onEpisodeSelected(episodeNum)
                    navController.navigate(screen.route)
                },
                locationNavigate = { screen: Screens, locationId: Int ->
                    onLocationSelected(locationId)
                    navController.navigate(screen.route)
                }
            )
        }
        composable(Screens.EpisodeScreen.route) {
            EpisodeScreen(episodeScreenState)
        }
        composable(Screens.LocationScreen.route) {
            LocationScreen(locationScreenState)
        }
    }
}
