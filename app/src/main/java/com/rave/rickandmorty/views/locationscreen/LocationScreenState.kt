package com.rave.rickandmorty.views.locationscreen

import com.rave.rickandmorty.model.local.Location

/**
 * Location screen state.
 *
 * @property isLoading
 * @property location
 * @property errMsg
 * @constructor Create empty Location screen state
 */
data class LocationScreenState(
    val isLoading: Boolean = false,
    val location: Location? = null,
    val errMsg: String = "Error encountered"
)
