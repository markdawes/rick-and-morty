package com.rave.rickandmorty.views.characterscreen

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import coil.compose.AsyncImage
import com.rave.rickandmorty.model.local.RickCharacter
import com.rave.rickandmorty.ui.theme.LightBlue
import com.rave.rickandmorty.views.Screens

/**
 * Character screen.
 *
 * @param state
 * @param navigate
 * @receiver
 */
@Composable
fun CharacterScreen(
    state: CharacterScreenState,
    navigate: (screen: Screens, rickCharacter: RickCharacter) -> Unit
) {
    LazyColumn(modifier = Modifier.background(LightBlue)) {
        items(state.characters) { rickCharacter: RickCharacter ->
            Row(
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceAround,
                modifier = Modifier.clickable {
                    navigate(Screens.CharacterDetailsScreen, rickCharacter)
                }.background(LightBlue)
            ) {
                AsyncImage(
                    model = rickCharacter.image,
                    contentDescription = "${rickCharacter.name} shown as an image."
                )
                Text(
                    text = rickCharacter.name,
                    color = Color.White
                )
            }
        }
    }
}
