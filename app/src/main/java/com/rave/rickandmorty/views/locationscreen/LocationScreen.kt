package com.rave.rickandmorty.views.locationscreen

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import com.rave.rickandmorty.ui.theme.LightBlue

@Composable
fun LocationScreen(state: LocationScreenState) {
    Column(
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.background(LightBlue)
    ) {
        Text(text = "Location details for ${state.location?.name}")
        Text(text = "Location is in dimension ${state.location?.dimension}")
        @Suppress("MagicNumber")
        Text(text = "Location was created on ${state.location?.created?.take(10)}")
        Text(text = "Location is of type ${state.location?.type}")
    }
}
