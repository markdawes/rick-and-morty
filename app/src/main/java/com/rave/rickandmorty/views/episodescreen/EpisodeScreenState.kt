package com.rave.rickandmorty.views.episodescreen

import com.rave.rickandmorty.model.local.Episode

/**
 * Episode screen state.
 *
 * @property isLoading
 * @property episode
 * @property errMsg
 * @constructor Create empty Episode screen state
 */
data class EpisodeScreenState(
    val isLoading: Boolean = false,
    val episode: Episode? = null,
    val errMsg: String = "Error encountered"
)
