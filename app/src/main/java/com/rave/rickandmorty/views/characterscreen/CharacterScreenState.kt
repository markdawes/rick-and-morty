package com.rave.rickandmorty.views.characterscreen

import com.rave.rickandmorty.model.local.RickCharacter

/**
 * Character screen state.
 *
 * @property isLoading
 * @property characters
 * @property error
 * @constructor Create empty Character screen state
 */
data class CharacterScreenState(
    val isLoading: Boolean = false,
    val characters: List<RickCharacter> = emptyList(),
    val error: String = "Error encountered"
)
