package com.rave.rickandmorty.views.characterdetailsscreen

import com.rave.rickandmorty.model.local.RickCharacter

/**
 * Character details state.
 *
 * @property isLoading
 * @property rickCharacter
 * @property errMsg
 * @constructor Create empty Character details state
 */
data class CharacterDetailsState(
    val isLoading: Boolean = false,
    val rickCharacter: RickCharacter? = null,
    val errMsg: String = "Error encountered"
)
