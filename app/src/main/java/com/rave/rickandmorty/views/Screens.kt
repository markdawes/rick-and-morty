package com.rave.rickandmorty.views

/**
 * Screens.
 *
 * @property route
 * @constructor Create empty Screens
 */
enum class Screens(val route: String) {
    CharacterScreen("CharacterScreen"),
    CharacterDetailsScreen("CharacterDetailsScreen"),
    EpisodeScreen("EpisodeScreen"),
    LocationScreen("LocationScreen")
}
