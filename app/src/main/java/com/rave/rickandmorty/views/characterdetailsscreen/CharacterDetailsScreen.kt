package com.rave.rickandmorty.views.characterdetailsscreen

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import coil.compose.AsyncImage
import com.rave.rickandmorty.ui.theme.LightBlue
import com.rave.rickandmorty.views.Screens

/**
 * Character details screen.
 *
 * @param state
 * @param navigate
 * @param locationNavigate
 * @receiver
 * @receiver
 */
@Composable
fun CharacterDetailsScreen(
    state: CharacterDetailsState,
    navigate: (screen: Screens, episodeNum: Int) -> Unit,
    locationNavigate: (screen: Screens, locationId: Int) -> Unit
) {
    Column(
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.background(LightBlue)
    ) {
        Text(text = "Details for ${state.rickCharacter?.name}")
        AsyncImage(
            model = state.rickCharacter?.image,
            contentDescription = "${state.rickCharacter?.name} shown as an image."
        )
        Text(text = "Gender is ${state.rickCharacter?.gender}")
        Text(text = "Species is ${state.rickCharacter?.species}")
        Text(text = "Status is ${state.rickCharacter?.status}")
        @Suppress("MagicNumber")
        Text(text = "Created on ${state.rickCharacter?.created?.take(10)}")
        Text(
            text = "\nPrimary location is ${state.rickCharacter?.location?.get("name")}",
            modifier = Modifier.clickable {
                locationNavigate(
                    Screens.LocationScreen,
                    getTrailingNum(state.rickCharacter?.location?.get("url")!!)
                )
            }
        )
        Text(
            text = "\nFirst episode appearance is ${state.rickCharacter?.episode?.first()}",
            modifier = Modifier.clickable {
                navigate(
                    Screens.EpisodeScreen,
                    getTrailingNum(state.rickCharacter?.episode?.first()!!))
            }
        )
    }
}

/**
 * Get trailing num.
 *
 * @param url
 * @return
 */
fun getTrailingNum(url: String): Int {
    var numSlashes = 0
    var chars: MutableList<Char> = mutableListOf()
    for (c in url) {
        if (c == '/') {
            numSlashes++
            continue
        }
        @Suppress("MagicNumber")
        if (numSlashes > 4) {
            chars.add(c)
        }
    }
    return chars.toString()
        .trim('[').trim(']')
        .replace(",", "")
        .replace(" ", "")
        .toInt()
}
