package com.rave.rickandmorty.views.episodescreen

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import com.rave.rickandmorty.ui.theme.LightBlue

@Composable
fun EpisodeScreen(state: EpisodeScreenState) {
    Column(
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.background(LightBlue)
    ) {
        Text(text = "Here are details for episode ${state.episode?.id}")
        Text(text = "Episode name is ${state.episode?.name}")
        @Suppress("MagicNumber")
        Text(text = "Episode was created on ${state.episode?.created?.take(10)}")
        Text(text = "Episode aired on ${state.episode?.airDate}")
    }
}
